﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form1 : Form
    {
  
        public Form1()
        {
            InitializeComponent();
    
        

        }
           
        private Point RectStartPoint;
        private Rectangle Rect = new Rectangle();
        // private Brush selectionBrush = new SolidBrush(Color.FromArgb(128, 72, 145, 220));
        private Pen whitePen = new Pen(Color.White);

        private static bool Rectangle, mousepressed;
        Rectangle rec = new Rectangle(0, 0, 0, 0);



        protected int mx0, my0;  // point where the mouse down was clicked
        protected int mx1, my1;  // point where the mouse was released
        

        double xmax = 0.6;
        double xmin = -2.025;
        double ymax = 1.125;
        double ymin = -1.125;
        //value for number of maximun iteration 
        protected Boolean doDrawMandelbrot = true;  // should we draw the mandelbrot on this paint?
        public static int MaxIteration = 256;
        public Bitmap pic;
        public Bitmap zoom;
     
        private void Form1_Load(object sender, EventArgs e)
        {



            //passing the start and finish value for x and y cordinate
           pic = GenerateMandelbrotset(pictureBox1,xmax,xmin,ymax,ymin);

            //displays the image in the picture box
            pictureBox1.Image = pic;

            toolStripStatusLabel1.Text = "Mandlebrot Started";
        }

        static Bitmap GenerateMandelbrotset(PictureBox pictureBox1, double Xmax, double Xmin, double Ymax, double Ymin)
        {
            //giving position to the mandlebrot set
            double pXmax = Xmax;
            double pYmax = Ymax;
            double pXmin = Xmin;
            double pYmin = Ymin;

            //creating bitmap to draw the set
            Bitmap pic = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            double a = 0;
            double b = 0;
            double c = 0;
            double d = 0;

            //fill the whole window
            double Xwin = ((Xmax - Xmin) / Convert.ToDouble(pic.Width));
            double Ywin = ((Ymax - Ymin) / Convert.ToDouble(pic.Height));
            double temp = 0;

            //generates mandlebrot set
            int color = 0;
            for (int x = 0; x < pic.Width; x++)
            {
                c = (Xwin * x) - Math.Abs(Xmin);
                for (int y = 0; y < pic.Height; y++)
                {
                    a = 0;
                    b = 0;

                    d = (Ywin * y) - Math.Abs(Ymin);
                    color = 0;
                    //
                    while ((a * a - b * b) <= 3 && color < MaxIteration)
                    {
                        color++;
                        temp = a;
                        a = (a * a) - (b * b) + c;
                        b = (2 * temp * b) + d;
                    }

                   
                    //generating color pattern 
                    if (color < 60)
                    {
                        pic.SetPixel(x, y, Color.FromArgb(210,color + 196, color * 3 + 35, color));

                    }
                    else if (color < 120)
                    {
                        pic.SetPixel(x, y, Color.FromArgb(210,color + 80, color + 136, color-20));

                    }
                    else if (color < 180)
                    {
                        pic.SetPixel(x, y, Color.FromArgb(210,color+20, color + 70, color-50));

                    }
                    else if (color < MaxIteration)
                    {
                        pic.SetPixel(x, y, Color.FromArgb(210,color, color - 50, color));

                    }

                    else
                        pic.SetPixel(x, y, Color.Black);


                }
            }

            //returns bitmap value
            return pic;
        }


       
        private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e)
        {
          
           
    
            ////start point of rectangle 
               mx0 = e.X;
               my0 = e.Y;
            
               string cordx = mx0.ToString();
               string cordy = my0.ToString();
            //displaying start coordinates value in the status bar
               toolStripStatusLabel2.Text = "Start cord X= " + cordx + " Start cord Y = " + cordy;
               mousepressed = true;
               Invalidate();



        }

        private void pictureBox1_MouseMove_1(object sender, MouseEventArgs e)
        {
            
            if (mousepressed)
            {
               
              
                  //end point of rectangle
                    mx1 = e.X;                
                    my1 = e.Y;
                    Rectangle = true;
                    //repaint(); 

                    //displaying end coordinates value in the status bar

                    string cordx = mx1.ToString();
                    string cordy = my1.ToString();
                   toolStripStatusLabel3.Text = "End cord X= " + cordx + " End cord Y = " + cordy;   
                    pictureBox1.Invalidate();
                
            }
            try
            {
                //displaying RGB color value in the status bar
                Color c = pic.GetPixel(e.X, e.Y);
                string R = c.R.ToString();
                string G = c.G.ToString();
                string B = c.B.ToString();
                toolStripStatusLabel4.Text = "R = " + R + " G =" + G + " B = " + B;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox1_Paint_1(object sender, PaintEventArgs e)
        {

            //// Draw the rectangle...
            Graphics g = e.Graphics;
            g.DrawImage(pic, 0, 0,pictureBox1.Width, pictureBox1.Height);
            whitePen = new Pen(Color.White);
            if (mousepressed == true)
            {
                if (Rectangle)
                {
                    g.DrawRectangle(whitePen, rec);
                    Rectangle = true;
                    //g1.(Color.White);
                    if (mx0 < mx1)
                    {
                        if (my0 < my1)
                        {
                            g.DrawRectangle(whitePen, mx0, my0, (mx1 - mx0), (my1 - my0));
                            Invalidate();
                            //rectangle = false;
                        }

                        else g.DrawRectangle(whitePen, mx0, my1, (mx1 - mx0), (my0 - my1));
                    }
                    else
                    {
                        if (my0 < my1) g.DrawRectangle(whitePen, mx1, my0, (mx0 - mx1), (my1 - my0));
                        else g.DrawRectangle(whitePen, mx1, my1, (mx0 - mx1), (my0 - my1));
                    }
                }
                else if (mousepressed == false)
                {
                    Rectangle = false;
                }

            }
            else
            {
                g.DrawImageUnscaled(pic, 0, 0);

            }
            if (Rectangle == false)
            {
                Invalidate();
            }

            
            
            
        }
       
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {

            int z=0,w=0;
            double scaleX = 0; double scaleY=0;
            //end point of the rectangle
            mx1 = e.X;
            my1 = e.Y;

            if (mx0 > mx1)
            {
                z = mx0;
                mx0 = mx1;
                mx1 = z;
            }
            if(my0>my1)
            {
                z = my0;
                my0 = my1;
                my1 = z;
            }
            w = (mx1 - mx0);
            z = (my1 - my0);

            if ((w < 2) && (z < 2))
                MessageBox.Show("Zoom size not valid");

            else
            {
                scaleX = (xmax - xmin) / (double)pictureBox1.Width;
                scaleY = (ymax - ymin) / (double)pictureBox1.Height;

                //float xy = pictureBox1.Width / pictureBox1.Height;


                xmax = (double)mx1 * scaleX + xmin;
                ymax = (double)my1 * scaleY + ymin;
                xmin = (double)mx0 * scaleX + xmin;
                ymin = (double)my0 * scaleY + ymin;

                //if (((float)w > (float)z * xy)) my1 = (int)((float)my0 + (float)w / xy);
                //else mx1 = (int)((float)mx0 + (float)z * xy);
                //xmax = xmin + scaleX * (double)mx1;
                //ymax = ymin + scaleY * (double)my1;
                //xmin += scaleX * (double)mx0;
                //ymin += scaleY * (double)my0;

            }
         
            pic = GenerateMandelbrotset(pictureBox1, xmax, xmin, ymax, ymin);
            pictureBox1.Image = pic;
            Rectangle = false;
            mousepressed = false;
            Refresh();
            this.Invalidate();
          
         
            
          

        }

     
        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pic.Dispose();
            pic = GenerateMandelbrotset(pictureBox1, 0.6, -2.025, 1.125, -1.125);
            pictureBox1.Image = pic;
           
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.Cursor = Cursors.Cross;
        }

        

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Show();
            pic = GenerateMandelbrotset(pictureBox1, 0.6, -2.025, 1.125, -1.125);
            pictureBox1.Image = pic;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveimage = new SaveFileDialog())
            {
                saveimage.Title = "Save Dialog";
                saveimage.Filter = "Bitmap Images (*.bmp)|*.bmp|All files(*.*)|*.*";
                if (saveimage.ShowDialog(this) == DialogResult.OK)
                {
                    using (Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height))
                    {
                        pictureBox1.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                        bmp.Save(saveimage.FileName);
                        MessageBox.Show("Saved Successfully.....");

                    }
                }
            }
            
        }

        private void cloneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
        }

       

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void stopToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            
            pictureBox1.Hide();
            toolStripStatusLabel1.Text = "Mandlebrot Stopped";
        }

        private void reloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Developed And Designed By Saurav Shrestha");
        }


    }

    }
    

